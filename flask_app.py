from flask import Flask
from UnleashClient import UnleashClient

app = Flask(__name__)

def getFeatureFlag(flagName):
    client = UnleashClient(
        url="https://gitlab.com/api/v4/feature_flags/unleash/54093965",
        app_name = "All environments",
        instance_id="<your instance id>"
    )

    client.initialize_client()
    return client.is_enabled(flagName)

@app.route("/")
def feature_flag():    
    if getFeatureFlag("test-flag"):
        return "<p>Feature Flag is enabled</p>"
    else:
        return "<p>Feature Flag is disabled</p>"
    