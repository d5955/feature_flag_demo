from UnleashClient import UnleashClient

client = UnleashClient(
    url="https://gitlab.com/api/v4/feature_flags/unleash/54093965",
    app_name = "All environments",
    instance_id="<your instance id>"

)

client.initialize_client()
print(client.is_enabled("test-flag"))